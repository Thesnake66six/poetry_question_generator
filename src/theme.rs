use super::title_words::NON_CAPS_WORDS;

#[derive(Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Clone)]
pub struct Theme {
    name: String,
}

impl Theme {
    pub fn new(s: String) -> Theme {
        let s = s.trim().to_lowercase();
        Theme { name: s }
    }

    pub fn name_lowercase(&self) -> String {
        self.name.clone()
    }

    pub fn name_firstcap(&self, mut always: bool) -> String {
        let mut done_flag = false;
        self.name
            .clone()
            .split(' ')
            .map(|x| {
                if (!NON_CAPS_WORDS.contains(&x) && !done_flag) || always {
                    done_flag = true;
                    always = false;
                    let c = x.split_at(1);
                    c.0.to_uppercase() + c.1
                } else {
                    x.to_owned()
                }
            })
            .collect::<Vec<String>>()
            .join(" ")
    }

    pub fn name_titlecase(&self, mut first: bool) -> String {
        self.name
            .clone()
            .split(' ')
            .map(|x| {
                if !NON_CAPS_WORDS.contains(&x) || first {
                    first = false;
                    let c = x.split_at(1);
                    c.0.to_uppercase() + c.1
                } else {
                    x.to_owned()
                }
            })
            .collect::<Vec<String>>()
            .join(" ")
    }
}

impl From<String> for Theme {
    fn from(value: String) -> Self {
        Theme::new(value)
    }
}
