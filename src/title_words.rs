pub const NON_CAPS_WORDS: [&str; 32] = [
    "a", "and", "as", "at", "but", "by", "down", "extract", "for", "from", "if", "in", "into",
    "near", "nor", "of", "off", "on", "once", "onto", "or", "over", "past", "so", "than", "that",
    "the", "to", "upon", "when", "with", "yet",
];
