use std::{
    collections::{BTreeSet, HashSet},
    fs::{read_to_string, File},
    path::Path,
};

use crate::{
    poem::{self, Poem},
    theme::Theme,
};

pub fn parse_poem(filepath: &Path) -> Poem {
    let _ = match File::open(filepath) {
        Err(why) => panic!(
            "couldn't open {} while parsing poem: {}",
            filepath.display(),
            why
        ),
        Ok(file) => file,
    };

    let name = filepath
        .file_stem()
        .unwrap()
        .to_owned()
        .into_string()
        .unwrap();
    let mut poem = Poem::new(name);

    println!("\x1b[1;94mBuilding\x1b[0m {}", poem.name_titlecase(true));

    let lines = read_to_string(filepath)
        .unwrap()
        .lines()
        .map(String::from)
        .collect::<Vec<String>>();

    for mut line in lines {
        line = line.trim().to_string();
        if line.starts_with('#') || line.is_empty() {
            continue;
        } else {
            poem.themes.insert(line.into());
        }
    }

    poem
}
