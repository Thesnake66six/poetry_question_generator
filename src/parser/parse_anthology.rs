use std::{fs, path::Path};

use crate::{anthology::Anthology, parser::parse_poem::parse_poem};

pub fn parse_anthology(filepath: &Path) -> Anthology {
    let name = filepath
        .file_name()
        .unwrap()
        .to_owned()
        .into_string()
        .unwrap();

    let mut anthology = Anthology::new(name);

    // if let Ok(entries) = fs::read_dir(filepath) {
    //     for entry in entries {
    //         if let Ok(entry) = entry {
    //             if entry.file_type().unwrap().is_file() {
    //                 dbg!(entry.path());
    //                 poems.insert(parse_poem(&entry.path()));
    //             }
    //         }
    //     }
    // }

    println!(
        "\x1b[1;92mConverting\x1b[0m {}",
        anthology.name_titlecase(true)
    );

    match fs::read_dir(filepath) {
        Err(why) => panic!(
            "couldn't open {} while parsing anthology: {}",
            filepath.display(),
            why
        ),
        Ok(entries) => {
            for entry in entries {
                match entry {
                    Err(why) => panic!(
                        "couldn't open {} while parsing anthology: {}",
                        filepath.display(),
                        why
                    ),
                    Ok(entry) => {
                        if entry.file_type().unwrap().is_file() {
                            anthology.poems.insert(parse_poem(&entry.path()));
                        }
                    }
                }
            }
        }
    }

    let themes = anthology.themes_counts();
    for theme in &themes {
        if *theme.1 < 2 {
            let suspicious_poem = anthology
                .get_poems_with_theme(theme.0)
                .into_iter()
                .next()
                .unwrap();
    
            println!(
                "\x1b[1;93mWarning:\x1b[0m theme \"{}\" only appears once, in poem \"{}\".",
                theme.0.name_titlecase(true),
                suspicious_poem.name_titlecase(false)
            );
        }
        else if *theme.1 < 3 {
            let mut suspicious_poems = anthology
                .get_poems_with_theme(theme.0)
                .into_iter();
    
            println!(
                "\x1b[1;93mWarning:\x1b[0m theme \"{}\" only appears twice, in poems \"{}\" and \"{}\".",
                theme.0.name_titlecase(true),
                suspicious_poems.next().unwrap().name_titlecase(false),
                suspicious_poems.next().unwrap().name_titlecase(false)
            );
        }
    }

    println!(
        "\x1b[1;92mConverted\x1b[0m {}",
        anthology.name_titlecase(true)
    );
    anthology
}
