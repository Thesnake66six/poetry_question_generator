use std::fs;
use std::path::Path;
use std::result::Result::Ok;
use std::io::{self, Write};

use anthology::Anthology;

use crate::{parser::parse_anthology::parse_anthology, poem::Poem, theme::Theme};

mod anthology;
mod parser;
mod poem;
mod theme;
mod title_words;

macro_rules! read_line {
    () => {{
        print!("> ");
        let _ = io::stdout().flush();
        let mut str = String::new();
        std::io::stdin().read_line(&mut str).expect("!");
        str = str.trim().to_string();
        str
    }};
}

fn main() -> anyhow::Result<()> {
    print!("\x1b[2J");
    let anthologies = load_anthologies();

    println!();
    println!("/------------/");
    println!();

    let mut anthology = select_anthology_prompt(&anthologies);

    wait("Press Enter to continue.");

    loop {
        print!("\x1b[2J");
        println!();
        println!("To select question type, enter:");
        println!();

        println!("1 for random poem, random theme");
        println!("2 for random poem, random theme, random poem");
        println!("3 for select poem, random theme");
        println!("4 for select poem, random theme, random poem");
        println!("5 for select poem, select theme, random poem");

        println!();
        println!("Or, enter:");
        println!();
        println!("6 for smallest poems set covering all themes at least once");
        println!("7 for smallest poems set covering all themes at least twice");
        println!("8 to list all themes in the anthology");
        println!("9 to list all themes in a poem");
        println!("10 to list all poems containing a theme");
        println!();
        println!("11 to switch anthologies");
        println!();
        println!("(Leave blank to quit)");

        let input: usize;
        loop {
            let i = &read_line!();
            if i.is_empty() {
                std::process::exit(0)
            }
            match i.parse::<usize>() {
                Ok(i) => {
                    input = i;
                    break;
                }
                Err(_) => continue,
            }
        }

        let set: Option<(Poem, Theme, Option<Poem>)>;

        match input {
            1 => {
                let x = anthology.poem_theme().unwrap();
                set = Some((x.0, x.1, None))
            }
            2 => {
                let x = anthology.poem_theme_poem().unwrap();
                set = Some((x.0, x.1, Some(x.2)))
            }
            3 => {
                let p = select_poem_prompt(&anthology);
                let x = anthology.given_theme(p).unwrap();
                set = Some((p.clone(), x, None))
            }
            4 => {
                let p = select_poem_prompt(&anthology);
                let x = anthology.given_theme_poem(p).unwrap();
                set = Some((p.clone(), x.0, Some(x.1)))
            }
            5 => {
                let t = select_theme_prompt(&anthology);
                let x = anthology.poem_given_poem(&t).unwrap();
                set = Some((x.0, t, Some(x.1)))
            }
            6 => {
                print!("\x1b[2J");
                println!("The set:");
                println!();
                let s = anthology.sets(1);
                let mut count = s.len() - 1;
                for p in s {
                    let collect = &mut p.iter().collect::<Vec<_>>();
                    collect.sort();
                    for c in collect {
                        println!("\"{}\"", c.name_titlecase(true))
                    }
                    if count > 0 {
                        println!();
                        println!("Or:");
                        println!();
                    }
                    count = count.saturating_sub(1);
                }
                wait("Press Enter to return.");
                continue;
            }
            7 => {
                print!("\x1b[2J");
                println!("The set:");
                println!();
                let s = anthology.sets(2);
                let mut count = s.len() - 1;
                for p in s {
                    for c in p {
                        println!("\"{}\"", c.name_titlecase(true))
                    }
                    if count > 0 {
                        println!();
                        println!("Or:");
                        println!();
                    }
                    count = count.saturating_sub(1);
                }
                wait("Press Enter to return.");
                continue;
            }
            8 => {
                print!("\x1b[2J");
                println!(
                    "Themes in the {} anthology:",
                    anthology.name_titlecase(true)
                );
                for theme in anthology.themes() {
                    println!("{}", theme.name_titlecase(true))
                }
                wait("Press Enter to return.");
                continue;
            }
            9 => {
                let p = select_poem_prompt(&anthology);
                print!("\x1b[2J");
                println!("Themes in \"{}\":", p.name_titlecase(true));
                for theme in &p.themes {
                    println!("{}", theme.name_titlecase(true))
                }
                wait("Press Enter to return.");
                continue;
            }
            10 => {
                let t = select_theme_prompt(&anthology);
                print!("\x1b[2J");
                println!("Poems with the theme \"{}\":", t.name_titlecase(true));
                for poem in anthology.get_poems_with_theme(&t) {
                    println!("{}", poem.name_titlecase(true))
                }
                wait("Press Enter to return.");
                continue;
            }
            11 => {
                print!("\x1b[2J");
                anthology = select_anthology_prompt(&anthologies);
                wait("Press Enter to return.");
                continue;
            }
            _ => {
                panic!()
            }
        }

        let set = set.unwrap();

        print!("\x1b[2J");
        match set.2 {
            Some(t) => {
                println!(
                    "Compare the theme of {} in the poems \"{}\", and \"{}\".",
                    set.1.name_lowercase(),
                    set.0.name_titlecase(true),
                    t.name_titlecase(true)
                );
            }
            None => {
                println!(
                    "Compare the theme of {} in the poem \"{}\", and one other poem from the {} anthology.",
                    set.1.name_lowercase(),
                    set.0.name_titlecase(true),
                    anthology.name_titlecase(false)
                );
            }
        }
        wait("Press Enter to return.")
    }
}

fn select_anthology_prompt(anthologies: &Vec<Anthology>) -> Anthology {
    println!("Select anthology to load:");

    for (i, p) in anthologies.iter().enumerate() {
        println!("{} for \"{}\"", i + 1, p.name_titlecase(true))
    }
    let input: usize;
    loop {
        match read_line!().parse::<usize>() {
            Ok(i) => {
                input = i - 1;
                break;
            }
            Err(_) => continue,
        }
    }

    anthologies.iter().collect::<Vec<_>>()[input].clone()
}
fn select_poem_prompt(anthology: &Anthology) -> &Poem {
    print!("\x1b[2J");
    println!("Enter:");

    for (i, p) in anthology.poems.iter().enumerate() {
        println!("{} for \"{}\"", i + 1, p.name_titlecase(true))
    }
    let input: usize;
    loop {
        match read_line!().parse::<usize>() {
            Ok(i) => {
                input = i - 1;
                break;
            }
            Err(_) => continue,
        }
    }
    anthology.poems.iter().collect::<Vec<_>>()[input]
}
fn select_theme_prompt(anthology: &Anthology) -> Theme {
    print!("\x1b[2J");
    println!("Enter:");
    let themes = anthology.themes();

    for (i, p) in themes.iter().enumerate() {
        println!("{} for \"{}\"", i + 1, p.name_titlecase(true))
    }
    let input: usize;
    loop {
        match read_line!().parse::<usize>() {
            Ok(i) => {
                input = i - 1;
                break;
            }
            Err(_) => continue,
        }
    }
    themes.iter().collect::<Vec<_>>()[input].clone()
}
fn wait(msg: &str) {
    println!();
    println!("{}", msg);
    read_line!();
}

fn load_anthologies() -> Vec<Anthology> {
    let filepath = Path::new("./");
    let mut anthologies = Vec::new();

    match fs::read_dir(filepath) {
        Err(why) => panic!(
            "couldn't open {} while parsing anthologies: {}",
            filepath.display(),
            why
        ),
        Ok(entries) => {
            for entry in entries {
                match entry {
                    Err(why) => panic!(
                        "couldn't open {} while parsing anthologies: {}",
                        filepath.display(),
                        why
                    ),
                    Ok(entry) => {
                        if entry.file_type().unwrap().is_dir()
                            && *entry.path().file_name().unwrap() != *"src".to_owned()
                            && *entry.path().file_name().unwrap() != *"target".to_owned()
                            && *entry.path().file_name().unwrap() != *".git".to_owned()
                        {
                            let a = parse_anthology(&entry.path());
                            anthologies.insert(0, a);
                        }
                    }
                }
            }
        }
    }

    println!(
        "\x1b[1;92mDone:\x1b[0m {} anthologies loaded",
        anthologies.len()
    );
    anthologies
}
