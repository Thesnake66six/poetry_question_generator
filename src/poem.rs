use super::{theme::Theme, title_words::NON_CAPS_WORDS};
use std::{
    collections::{BTreeSet, HashSet},
    fmt::Display,
};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]

pub struct Poem {
    pub name: String,
    pub themes: BTreeSet<Theme>,
}

impl Poem {
    pub fn new(s: String) -> Poem {
        let s = s.trim().to_lowercase();
        Poem {
            name: s,
            themes: BTreeSet::new(),
        }
    }

    pub fn name_lowercase(&self) -> String {
        self.name.clone()
    }

    pub fn name_firstcap(&self, mut always: bool) -> String {
        let mut done_flag = false;
        self.name
            .clone()
            .split(' ')
            .map(|x| {
                if (!NON_CAPS_WORDS.contains(&x) && !done_flag) || always {
                    done_flag = true;
                    always = false;
                    let c = x.split_at(1);
                    c.0.to_uppercase() + c.1
                } else {
                    x.to_owned()
                }
            })
            .collect::<Vec<String>>()
            .join(" ")
    }

    pub fn name_titlecase(&self, mut first: bool) -> String {
        self.name
            .clone()
            .split(' ')
            .map(|x| {
                if !NON_CAPS_WORDS.contains(&x) || first {
                    first = false;
                    let c = x.split_at(1);
                    c.0.to_uppercase() + c.1
                } else {
                    x.to_owned()
                }
            })
            .collect::<Vec<String>>()
            .join(" ")
    }
}

impl From<String> for Poem {
    fn from(value: String) -> Self {
        Poem::new(value)
    }
}

impl Display for Poem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&(self.name_titlecase(true) + "\n")).unwrap();
        for theme in &self.themes {
            f.write_str(&(">   ".to_owned() + &theme.name_titlecase(true) + "\n"))
                .unwrap()
        }

        Ok(())
    }
}
