use std::{
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    fmt::Display,
};

use crate::theme;

use super::{poem::Poem, theme::Theme, title_words::NON_CAPS_WORDS};

#[derive(Debug, Clone)]
pub struct Anthology {
    pub name: String,
    pub poems: BTreeSet<Poem>,
}

impl Anthology {
    pub fn new(s: String) -> Anthology {
        let s = s.trim().to_lowercase();
        Anthology {
            name: s,
            poems: BTreeSet::new(),
        }
    }

    pub fn name_lowercase(&self) -> String {
        self.name.clone()
    }

    pub fn name_firstcap(&self, mut always: bool) -> String {
        let mut done_flag = false;
        self.name
            .clone()
            .split(' ')
            .map(|x| {
                if (!NON_CAPS_WORDS.contains(&x) && !done_flag) || always {
                    done_flag = true;
                    always = false;
                    let c = x.split_at(1);
                    c.0.to_uppercase() + c.1
                } else {
                    x.to_owned()
                }
            })
            .collect::<Vec<String>>()
            .join(" ")
    }

    pub fn name_titlecase(&self, mut first: bool) -> String {
        self.name
            .clone()
            .split(' ')
            .map(|x| {
                if !NON_CAPS_WORDS.contains(&x) || first {
                    first = false;
                    let c = x.split_at(1);
                    c.0.to_uppercase() + c.1
                } else {
                    x.to_owned()
                }
            })
            .collect::<Vec<String>>()
            .join(" ")
    }

    pub fn themes(&self) -> BTreeSet<Theme> {
        let mut themes = BTreeSet::new();
        for poem in &self.poems {
            for theme in &poem.themes {
                themes.insert(theme.clone());
            }
        }
        themes
    }

    pub fn theme_count(&self, theme: Theme) -> usize {
        let mut count = 0;
        for poem in &self.poems {
            if poem.themes.contains(&theme) {
                count += 1;
            }
        }
        count
    }

    pub fn themes_counts(&self) -> HashMap<Theme, usize> {
        let mut themes = HashMap::new();
        for theme in self.themes() {
            themes.insert(theme.clone(), 0);
        }
        for poem in &self.poems {
            for theme in &poem.themes {
                *themes.get_mut(theme).unwrap() += 1;
            }
        }
        themes
    }

    pub fn get_poems_with_theme(&self, theme: &Theme) -> HashSet<Poem> {
        let mut poems_with_theme = HashSet::new();
        for poem in &self.poems {
            if poem.themes.contains(theme) {
                poems_with_theme.insert(poem.clone());
            }
        }
        poems_with_theme
    }

    pub fn poem_theme(&self) -> Option<(Poem, Theme)> {
        let poem = fastrand::choice(self.poems.iter()).unwrap().clone();
        let binding = poem.clone();
        let theme = fastrand::choice(binding.themes.iter()).unwrap();
        Some((poem, theme.clone()))
    }

    pub fn poem_theme_poem(&self) -> Option<(Poem, Theme, Poem)> {
        let poem = fastrand::choice(self.poems.iter()).unwrap().clone();
        let binding = poem.clone();
        let theme = fastrand::choice(binding.themes.iter()).unwrap();

        let mut poem2 = self.poems.clone();
        poem2.retain(|x| x.themes.contains(theme) && *x != poem);

        Some((poem, theme.clone(), fastrand::choice(poem2).unwrap()))
    }
    pub fn given_theme(&self, poem: &Poem) -> Option<Theme> {
        let poem = poem.clone();
        let binding = poem.clone();
        let theme = fastrand::choice(binding.themes.iter()).unwrap();
        Some(theme.clone())
    }

    pub fn given_theme_poem(&self, poem: &Poem) -> Option<(Theme, Poem)> {
        let poem = poem.clone();
        let binding = poem.clone();
        let theme = fastrand::choice(binding.themes.iter()).unwrap();

        let mut poem2 = self.poems.clone();
        poem2.retain(|x| x.themes.contains(theme) && *x != poem);

        Some((theme.clone(), fastrand::choice(poem2).unwrap()))
    }

    pub fn poem_given_poem(&self, theme: &Theme) -> Option<(Poem, Poem)> {
        let mut candidates = self.poems.clone();
        candidates.retain(|x| x.themes.contains(&theme));

        let x = fastrand::choose_multiple(candidates.iter(), 2);

        Some((x[0].clone(), x[1].clone()))
    }

    pub fn sets(&self, min_occurrences: usize) -> BTreeSet<BTreeSet<Poem>> {
        let mut best_set_size = usize::MAX;
        let mut best_sets: BTreeSet<BTreeSet<Poem>> = BTreeSet::new();
        let theme_number = self.themes().len();

        // Iterate over everey starting theme
        for i in 0..theme_number {
            // Move to the next starting theme
            let variants = self.themes();
            let mut themes = variants.iter().cycle();
            let mut themes = themes.skip(i);

            let mut covered_themes: BTreeMap<Theme, usize> = BTreeMap::new();
            for t in self.themes() {
                covered_themes.insert(t, 0);
            }
            let mut selected_poems: BTreeSet<Poem> = BTreeSet::new();

            // For every theme
            for _ in 0..theme_number {
                let theme = themes.next().unwrap();
                // If the theme has been covered an adequate number of times, skip itf
                if *covered_themes.get(theme).unwrap() >= min_occurrences {
                    continue;
                }
                let mut poem_candidates = self.poems.clone();
                poem_candidates.retain(|x| x.themes.contains(theme) && !selected_poems.contains(x));
                let mut poem_candidate = Poem::new("The Null Poem".to_owned());
                let mut poem_candidate_number = 0;
                for p in poem_candidates {
                    let mut tl = p.themes.clone();
                    tl.retain(|x| !covered_themes.get(x).unwrap() >= min_occurrences);
                    if tl.len() >= poem_candidate_number {
                        poem_candidate_number = tl.len();
                        poem_candidate = p
                    }
                }
                for t in &poem_candidate.themes {
                    *covered_themes.get_mut(t).unwrap() += 1;
                }
                selected_poems.insert(poem_candidate);
            }

            let candidate_set = selected_poems;
            let candidate_set_size = candidate_set.len();
            if candidate_set_size < best_set_size {
                best_sets = BTreeSet::from([candidate_set]);
                best_set_size = candidate_set_size;
            } else if candidate_set_size == best_set_size {
                for best_set in &best_sets {
                    if candidate_set == *best_set {
                        continue;
                    }
                }
                best_sets.insert(candidate_set);
            }
        }

        best_sets
    }
}

impl Display for Anthology {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&(self.name_titlecase(true) + "\n")).unwrap();
        for poem in &self.poems {
            f.write_str("\n").unwrap();
            f.write_str(&format!("{}", poem)).unwrap();
        }
        Ok(())
    }
}
